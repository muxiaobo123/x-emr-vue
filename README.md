## 概要

基于HTML5的电子病历编辑器的Vue版演示程序。

![image](https://gitee.com/bensenplus/x-emr-vue/raw/master/demo.png)

---

## 如何使用

编辑器使用说明参考 [www.x-emr.cn](https://www.x-emr.cn)

### 安装

```sh
npm install
```

### 运行

```sh
npm run dev
```

### 编译

```sh
npm run build
```

搭建Vue环境,参考 [www.vuepress.cn](https://www.vuepress.cn/guide/getting-started.html)
